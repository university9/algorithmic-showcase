import sys

sys.path.insert(0, r'..\Model\Algorithms')
sys.path.insert(0, r'..\Model\Algorithms\GreedyAlgorithms')
sys.path.insert(0, r'..\Model\Algorithms\DnCAlgorithms')
sys.path.insert(0, r'..\Model\Algorithms\DynamicAlgorithms')
sys.path.insert(0, r'..\Model\Algorithms\GeneticAlgorithm')
sys.path.insert(0, r'..\Model\Algorithms\ProbabilisticAlgorithms')

from AbstractAlgorithm import *
from ShortestSuperString import *
from Boruvka import *
from Backpack import *
from LargestPalindrome import *
from ArithmeticProgression import *
from FourierTransformation import *
from StrassenMultiplication import *
from ConvexHull import *
from MSIS import *
from MMVAE import *
from DistinctArrays import *
from TravellingSalesman import*
from EvolutionOfMonaLisa import*
from Freivald import*
from Quicksort import *
from LVQueens import *
from PrimalityTest import *

class AlgorithmFactory:

    def __init__(self):
        self.adictionary = {
                            "ShortestSuperString": ShortestSuperString(),
                            "Backpack": Backpack(),
                            "Boruvka": Boruvka(),
                            "LargestPalindrome": LargestPalindrome(),
                            "ArithmeticProgression": ArithmeticProgression(),
                            "ConvexHull": ConvexHull(),
                            "FourierTransformation": FFTPM(),
                            "StrassenMultiplication": StrassenMultiplication(),
                            "MSIS": MSIS(),
                            "MMVAE": MMVAE(),
                            "DistinctArrays": DistinctArrays(),
                            "TravellingSalesman":TravellingSalesman(),
                            "EvolutionOfMonaLisa":EvolutionOfMonaLisa(),
                            "Freivald":Freivald(),
                            "Quicksort":Quicksort(),
                            "LVQueens":LVQueens(),
                            "PrimalityTest":PrimalityTest()
                            
                            }

    def get_algorithm(self, name):
        return self.adictionary[name]









