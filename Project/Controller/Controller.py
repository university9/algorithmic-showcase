from FileManager import *
from AlgorithmFactory import *


class Controller:

    def __init__(self):
        self.factory = AlgorithmFactory()
        self.manager = FileManager()

    def execute_algorithm(self, name):
        return self.factory.get_algorithm(name).execute(self.manager.open_file(name))

algoritmos=["PrimalityTest","LVQueens","Quicksort","Freivald"]
a=Controller()
for n in algoritmos:
    print("######## "+n+ " ########" )
    print(a.execute_algorithm(n))
    print()
