import sys
sys.path.insert(0, r'..' )
from AbstractAlgorithm import *


class ArithmeticProgression(AbstractAlgorithm):

    def __init__(self):
        pass

    def execute(self, data):
        print("Instance of problem:")
        print(data)
        print("The missing number:")
        correctData=eval(data)
        print( self.findMissingNumber(correctData))

    def findMissingNumber(self,array):
        diff=(array[len(array)-1]-array[0])//(len(array))
        if(array[0]+diff*len(array) != array[len(array)-1]):
            return "the sequence is OK"
        return self.findMissingNumberAUX(array,diff)

    def findMissingNumberAUX(self,array, diff):
        if(len(array)<= 1):
            return "the sequence is OK"
        mid = len(array)//2
        if(mid < (len(array)-1) and array[mid+1]-array[mid] != diff):
            return (array[mid]+diff)
        if( mid > 0 and array[mid] - array[mid - 1] != diff):
            return (array[mid - 1] + diff)
        if (array[mid] == array[0] + mid * diff):
            return self.findMissingNumberAUX(array[mid+1:len(array)], diff)
        return self.findMissingNumberAUX(array[0:mid],diff)
        


