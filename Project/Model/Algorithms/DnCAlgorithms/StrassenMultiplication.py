from builtins import range, len
import sys
sys.path.insert(0, r'..' )
from AbstractAlgorithm import *


class StrassenMultiplication(AbstractAlgorithm):

    def __init__(self):
        pass

    def execute(self, data):
        matrices = eval(data)
        matrixA = matrices[0]
        matrixB = matrices[1]
        print("Matriz A: ", matrixA)
        print("Matriz B: ", matrixB)
        print("Resultado: ", self.strassenMultiplication(matrixA, matrixB))


    def getBaseSize(self, matrix1, matrix2):
        baseSize1 = max(len(matrix1), len(matrix1[0]))
        baseSize2 = max(len(matrix2), len(matrix2[0]))
        finalSize = max(baseSize1, baseSize2)
        while(finalSize & (finalSize - 1) != 0):
            finalSize += 1
        return finalSize


    def enlargeMatrix(self, matrix, size):
        fillerList = []
        columnsNeeded = size - len(matrix[0])
        rowsNeeded = size - len(matrix)
        for i in range(len(matrix)):
            for j in range(columnsNeeded):
                matrix[i].append(0)
        for i in range(size):
            fillerList.append(0)
        for i in range(rowsNeeded):
            matrix.append(fillerList)
        return matrix

        
    def createSubMatrix(self, matrix, initialRow, initialColumn, size):
        resultMatrix = []
        i2 = initialRow
        for i in range(size):
            resultList = []
            j2 = initialColumn
            for j in range(size):
                resultList.append(matrix[i2][j2])
                j2 += 1
            resultMatrix.append(resultList)
            i2 += 1
        return resultMatrix


    def copySubArray(self, c, p, iB, jB):
        i2 = iB
        for i1 in range(len(c)):
            j2 = jB
            for j1 in range(len(c)):
                p[i2][j2] = c[i1][j1]
                j2 += 1
            i2 += 1


    def matrixSum(self, matrixA, matrixB):
        size = max(len(matrixA),len(matrixB))
        resultMatrix = []
        for i in range(size):
            answerList = []
            for j in range(size):
                answerList.append(matrixA[i][j] + matrixB[i][j])
            resultMatrix.append(answerList)
        return resultMatrix


    def matrixSubstraction(self, matrixA, matrixB):
        size = max(len(matrixA),len(matrixB))
        resultMatrix = []
        for i in range(size):
            answerList = []
            for j in range(size):
                answerList.append(matrixA[i][j] - matrixB[i][j])
            resultMatrix.append(answerList)
        return resultMatrix


    def insertSubMatrix(self, matrix, subMatrix, posRow, posColumn):
        for i in range(len(subMatrix)):
            for j in range(len(subMatrix)):
                matrix[posRow+i][posColumn+j] = subMatrix[i][j]
        return matrix


    def strassenMultiplication(self, matrixA, matrixB):

        matrixSize = self.getBaseSize(matrixA,matrixB)
        matrixA = self.enlargeMatrix(matrixA, matrixSize)
        matrixB = self.enlargeMatrix(matrixB, matrixSize)
        resultMatrix = [[0] * matrixSize for i in range(matrixSize)]
    
        if(matrixSize == 1):
            resultMatrix[0][0] = matrixA[0][0] * matrixB[0][0]

        else:
            subMatrixSize = matrixSize // 2
            A1 = self.createSubMatrix(matrixA, 0, 0, subMatrixSize)
            A2 = self.createSubMatrix(matrixA, 0, subMatrixSize, subMatrixSize)
            A3 = self.createSubMatrix(matrixA, subMatrixSize, 0, subMatrixSize)
            A4 = self.createSubMatrix(matrixA, subMatrixSize, subMatrixSize, subMatrixSize)

            B1 = self.createSubMatrix(matrixB, 0, 0, subMatrixSize)
            B2 = self.createSubMatrix(matrixB, 0, subMatrixSize, subMatrixSize)
            B3 = self.createSubMatrix(matrixB, subMatrixSize, 0, subMatrixSize)
            B4 = self.createSubMatrix(matrixB, subMatrixSize, subMatrixSize, subMatrixSize)

            p1 = self.strassenMultiplication(self.matrixSum(A1, A4), self.matrixSum(B1, B4))
            p2 = self.strassenMultiplication(self.matrixSum(A3, A4), B1)
            p3 = self.strassenMultiplication(A1, self.matrixSubstraction(B2, B4))       
            p4 = self.strassenMultiplication(A4, self.matrixSubstraction(B3, B1))
            p5 = self.strassenMultiplication(self.matrixSum(A1, A2), B4)
            p6 = self.strassenMultiplication(self.matrixSubstraction(A3, A1), self.matrixSum(B1, B2))
            p7 = self.strassenMultiplication(self.matrixSubstraction(A2, A4), self.matrixSum(B3, B4))

            C11 = self.matrixSum(self.matrixSubstraction(self.matrixSum(p1, p4), p5), p7)
            C12 = self.matrixSum(p3, p5)
            C21 = self.matrixSum(p2, p4)
            C22 = self.matrixSum(self.matrixSubstraction(self.matrixSum(p1, p3), p2), p6)

            self.copySubArray(C11, resultMatrix, 0, 0)
            self.copySubArray(C12, resultMatrix, 0, matrixSize // 2)
            self.copySubArray(C21, resultMatrix, matrixSize // 2, 0)
            self.copySubArray(C22, resultMatrix, matrixSize // 2, matrixSize // 2)
        
            resultMatrix = self.insertSubMatrix(resultMatrix, C11, 0, 0)
            resultMatrix = self.insertSubMatrix(resultMatrix, C12, 0, subMatrixSize)
            resultMatrix = self.insertSubMatrix(resultMatrix, C21, subMatrixSize, 0)
            resultMatrix = self.insertSubMatrix(resultMatrix, C22, subMatrixSize, subMatrixSize)

        return resultMatrix
