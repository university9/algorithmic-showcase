from cmath import exp, pi
import math
import sys

sys.path.insert(0, r'..' )
from AbstractAlgorithm import *

def FFT(A):
    
    n = len(A)
    
    if (n == 1):
        return A

    even = FFT(A[::2])
    odd  = FFT(A[1::2])

    y0 = []
    y1 = []
    
    for k in range(n//2):
        
        y0.append(even[k] + exp(-2*pi*k/n*1j)*odd[k]) 
        y1.append(even[k] - exp(-2*pi*k/n*1j)*odd[k])

    return  y0 + y1 

def iFFT(A):
    
    A = FFT([a.conjugate() for a in A])
    return [a.conjugate()/len(A) for a in A]

class FFTPM(AbstractAlgorithm):
    
    def __init__(self):
        pass

    def execute(self,data):
        data = eval(data)
        return self.apply(self.conv(data[0],data[1]))
    
    def circ_conv(self, A, B):
        
        n = len(A)
        A = FFT(A)
        B = FFT(B)
        C = [0]*(n)
        
        for i in range(n):
            C[i]=A[i]*B[i]
            
        return iFFT(C)

    def conv(self, A, B):
        
        n = len(A)
        m = len(B)
        N = 1
        
        while N < n+m-1:
            
            N *= 2
            
        A = A + [0]*(N-n)
        B = B + [0]*(N-m)
        C = self.circ_conv(A,B)
        
        return C[:n+m-1]

    def apply(self, A):
        print("La multiplicación de polinomios es: ", end = "")
        for i in range(len(A)):
            if i == len(A) - 1:
                print(str(round(A[i].real)) + "x^" + str(i))
            else:
                print((str(round(A[i].real))) + "x^" + str(i) + " + ", end = "")
            
    


