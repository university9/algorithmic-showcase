import sys
from matplotlib import pyplot as plt
from random import randint
from math import atan2

sys.path.insert(0, r'..')
from AbstractAlgorithm import *


class ConvexHull(AbstractAlgorithm):

    def __init__(self):
        pass

    def execute(self, data):
        tempData=eval(data)
        pts=self.create_points(tempData)
        print ("Points:",pts)
        hull=self.graham_scan(pts,True)
        print ("Hull:",hull)

    def create_points(self, ct,min=0,max=50):
            return [[randint(min,max),randint(min,max)] \
                            for _ in range(ct)]

    def polar_angle(self, p0,p1=None):
            if p1==None: p1=anchor
            y_span=p0[1]-p1[1]
            x_span=p0[0]-p1[0]
            return atan2(y_span,x_span)

    def distance(self,p0,p1=None):
            if p1==None: p1=anchor
            y_span=p0[1]-p1[1]
            x_span=p0[0]-p1[0]
            return y_span**2 + x_span**2

    def det(self,p1,p2,p3):
            return   (p2[0]-p1[0])*(p3[1]-p1[1]) \
                            -(p2[1]-p1[1])*(p3[0]-p1[0])

    def quicksort(self,a):
            if len(a)<=1: return a
            smaller,equal,larger=[],[],[]
            piv_ang=self.polar_angle(a[randint(0,len(a)-1)]) 
            for pt in a:
                    pt_ang=self.polar_angle(pt) 
                    if   pt_ang<piv_ang:  smaller.append(pt)
                    elif pt_ang==piv_ang: equal.append(pt)
                    else: 				  larger.append(pt)
            return   self.quicksort(smaller) \
                            +sorted(equal,key=self.distance) \
                            +self.quicksort(larger)

    def graham_scan(self,points,show_progress=False):
            global anchor
            min_idx=None
            for i,(x,y) in enumerate(points):
                    if min_idx==None or y<points[min_idx][1]:
                            min_idx=i
                    if y==points[min_idx][1] and x<points[min_idx][0]:
                            min_idx=i
            anchor=points[min_idx]
            sorted_pts=self.quicksort(points)
            del sorted_pts[sorted_pts.index(anchor)]

            hull=[anchor,sorted_pts[0]]
            for s in sorted_pts[1:]:
                    while self.det(hull[-2],hull[-1],s)<=0:
                            del hull[-1]
                    hull.append(s)
            return hull

    def benchmark(self,sizes=[10,100,1000,10000,100000]):
            for s in sizes:
                    tot=0.0
                    for _ in range(3):
                            pts=self.create_points(s,0,max(sizes)*10)
                            t0=time()
                            hull=self.graham_scan(pts,False)
                            tot+=(time()-t0)
                    print ("size %d time: %0.5f"%(s,tot/3.0))



