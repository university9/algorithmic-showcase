import sys

sys.path.insert(0, r'..')
from AbstractAlgorithm import *
from random import *

N = 2
  
class Freivald(AbstractAlgorithm):
    def __init__(self):
        pass

    def execute(self, data):
        print(data)
        tempData=eval(data)
        self.matrix = list(tempData)
        return self.isProduct(tempData)

    def freivald(self,a,b,c) : 
        r = [0] * N 
          
        for i in range(0, N) : 
            r[i] = (int)(random()) % 2
      
        br = [0] * N 
          
        for i in range(0, N) : 
            for j in range(0, N) : 
                br[i] = br[i] + b[i][j] * r[j] 
      
        cr = [0] * N 
        for i in range(0, N) : 
            for j in range(0, N) : 
                cr[i] = cr[i] + c[i][j] * r[j] 
      
        axbr = [0] * N 
        for i in range(0, N) : 
            for j in range(0, N) : 
                axbr[i] = axbr[i] + a[i][j] * br[j] 
      
        for i in range(0, N) : 
            if (axbr[i] - cr[i] != 0) : 
                return False
                  
        return True

    def isProduct(self, data) : 
        a=data[0]
        b=data[1]
        c=data[2]
        k=data[3] 
        for i in range(0, k) : 
            if (self.freivald(a, b, c) == False) :
                return False
        return True

