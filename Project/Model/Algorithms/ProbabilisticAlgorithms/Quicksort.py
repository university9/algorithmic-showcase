import sys
from random import randint

sys.path.insert(0, r'..')
from AbstractAlgorithm import *
from random import *

class Quicksort(AbstractAlgorithm):
    def __init__(self):
        pass

    def execute(self, data):
        print(data)
        tempData = eval(data)
        return self.quicksort(tempData)

    def quicksort(self, array):
        menores = []
        iguales = []
        mayores = []
        if len(array) > 1:
            indiceRandom = randint(0,(len(array) - 1))
            pivote = array[indiceRandom]
            for i in array:
                if i < pivote:
                    menores.append(i)
                elif i == pivote:
                    iguales.append(i)
                else:
                    mayores.append(i)
            return self.quicksort(menores) + iguales + self.quicksort(mayores)
        else:
            return array
