from random import sample
import sys


sys.path.insert(0, r'..')
from AbstractAlgorithm import *
from random import *

class LVQueens(AbstractAlgorithm):

    def __init__(self):
        self.size = 0
        self.rand = 0
        self.board = [-1] * self.size

    def execute(self,data):
        data = eval(data)
        self.size = data[0]
        self.rand = data[1]
        self.solve()
        return self.showBoard()

    def solve(self):
        if self.rand == 0:
            self.putQueens(0)
        else:
            while True:
                if self.LVBoard() == True:
                    break

    def LVBoard(self):
        positions = sample(range(0, self.size), self.rand)
        self.board = positions + (self.size - self.rand) * [-1]
        if self.feasible(0):
            return self.putQueens(self.rand - 1)
        else:
            return False

    def feasible(self, index):
        if (index == self.rand - 1):
            return True
        if self.isSafe(index, self.board[index]):
            return self.feasible(index + 1)
        return False

    def putQueens(self, row):
        if row == self.size:
            return True
        else:
            for column in range(self.size):
                if self.isSafe(row, column):
                    self.board[row] = column
                    if self.putQueens(row + 1):
                        return True
            return False

    def isSafe(self, row, column):
        for i in range(row):
            if self.board[i] == column or self.board[i] - i == column - row or self.board[i] + i == column + row:
                return False
        return True

    def showBoard(self):
        total = ""
        for row in range(self.size):
            line = ""
            for column in range(self.size):
                if self.board[row] == column:
                    line += "♛"
                else:
                    line += "♘"
                line += "  "
            total += line + "\n"
        return total
