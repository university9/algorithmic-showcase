import sys


sys.path.insert(0, r'..')
from AbstractAlgorithm import *
import random

N = 2
  
class PrimalityTest(AbstractAlgorithm):
    def __init__(self):
        pass

    def execute(self, data):
        print(data)
        tempData=eval(data)
        resultList=self.calculatePrimality(tempData)
        return resultList

    

    #maximo comun divisor
    def calculatePrimality(self,intList):
        resultList=[]
        for n in intList:
            resultList+=[self.primoMR(n,n*10)]
        return resultList

            
    def mcd(self,n,j):
            temp=0
            while(j!=0):
                    temp=j
                    j=n%j
                    n=temp
            return n
    def primoMR(self,numero,factor):#numero>=4
        compuesto=True
        k=0#veces que 2 divide a N
        r=numero-1
        a=0
        b=0
     
        while((r%2)==0):
            k=k+1
            r=r//2
        contador=1
        while(compuesto and (contador<factor)):
            a=random.randint(2,numero)
            if(self.mcd(a,numero)!=1):
                break
            else:
                b=(a**r)%numero
                if((b!=1 and b!=-1)):
                    for j in range (k-1):
                        b=(b**2)%numero
                compuesto= (b!=1)and (b!=-1)
            contador=contador+1
        return not compuesto
