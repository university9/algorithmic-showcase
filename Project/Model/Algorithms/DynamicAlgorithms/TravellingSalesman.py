from builtins import range
import sys
sys.path.insert(0, r'..' )
from AbstractAlgorithm import *

class TravellingSalesman(AbstractAlgorithm):
    def __init__(self):
        self.memory=[]

    def execute(self, data):
        graph = eval(data)
        print("instance of problem:")
        print(graph)
        print()
        print("Solution:")
        return self.salesmanProblem(graph)
    def salesmanProblem(self,graph):
    
        nodesList=list(range(1,len(graph)))
        finalList=self.salesmanProblemAUX(graph,0,nodesList)
        finalList=[finalList[0]]+[finalList[1]+[finalList[1][0]]]
            
        return finalList

    def salesmanProblemAUX(self,graph,node,listNodes):
        memo=self.binarySearch(self.memory, str(node)+","+str(listNodes))
        
        if ( memo!=-1):
            return [memo, [node]+listNodes]
                    
        if listNodes==[]:
            return [graph[node][0],[node]]
        else:
            array=[]
            for n in range(len(listNodes)):
                listR=self.salesmanProblemAUX(graph,listNodes[n],listNodes[0:n]+listNodes[n+1:len(listNodes)])
                array+=[[graph[node][listNodes[n]] + listR[0],[node]+listR[1]]]
        finalList = min(array)
        self.memory=self.binaryInsert(self.memory, [str(node)+","+str(listNodes),finalList[0]])
        return finalList


    def binaryInsert(self,listE, value):
        minn = 0 
        maxx = len(listE) -1
        if(listE==[]):
            return [value]
        if (value[0]>=listE[len(listE)-1][0]):
            return listE+[value]
        if (value[0]<=listE[0][0]):
            return [value]+listE
        while minn <= maxx:
            mid = (minn+maxx)//2
            if(mid < (len(listE)-1) and listE[mid][0]<value[0]<listE[mid+1][0]):
                return listE[0:mid+1]+[value]+listE[mid+1:len(listE)]
            if(mid > 0 and listE[mid][0]>value[0]>listE[mid-1][0]):
                return listE[0:mid]+[value]+listE[mid:len(listE)]
            

            elif listE[mid][0] > value[0]:
                maxx = mid-1
            else:
                minn = mid+1
        return "Not Added"
    def binarySearch(self,listE, value):
        minn = 0 
        maxx = len(listE) -1 
        while minn <= maxx:
            mid = (minn+maxx)//2
            if listE[mid][0] == value:
                return listE[mid][1]
            elif listE[mid][0] > value:
                maxx = mid-1
            else:
                minn = mid+1
        return -1
