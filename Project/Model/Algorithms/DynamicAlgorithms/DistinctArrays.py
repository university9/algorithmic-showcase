from builtins import range
import sys
sys.path.insert(0, r'..' )
from AbstractAlgorithm import *

class DistinctArrays(AbstractAlgorithm):
    def __init__(self):
        pass

    def execute(self, data):
        variables = eval(data)
        N = variables[0]
        M = variables[1]
        X = variables[2]
        return self.DistinctArrays(N, M, X)

    def DistinctArrays(self, N, M, X):
        totalWays = [[0] * (2) for i in range(N+1)]
        if(X == 1):
            totalWays[0][0] = 1
            totalWays[0][1] = 0
            totalWays[1][0] = 0
            totalWays[1][1] = (M - 1)
        else:
            totalWays[0][0] = 0
            totalWays[0][1] = (M - 1)
            totalWays[1][0] = 1
            totalWays[1][1] = (M - 2)
        for i in range(2,N):
            totalWays[i][0] = totalWays[i - 1][1]
            totalWays[i][1] = totalWays[i - 1][0] * (M - 1) + totalWays[i - 1][1] * (M - 2)
        return totalWays[N - 1][0]
