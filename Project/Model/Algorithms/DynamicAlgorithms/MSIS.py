import sys

sys.path.insert(0, r'..' )

from AbstractAlgorithm import *


class MSIS(AbstractAlgorithm):

    def __init__(self):
        pass

    def MSIS(self, arr):
        arrc = [arr[i] for i in range(len(arr))]

        for i in range(1, len(arr)):
            for j in range(i):
               if (arr[i] > arr[j] and arrc[i] < arrc[j] + arr[i]):
                   arrc[i] = arrc[j] + arr[i]

        return max(arrc)

    def execute(self, data):
        return self.MSIS(eval(data))



