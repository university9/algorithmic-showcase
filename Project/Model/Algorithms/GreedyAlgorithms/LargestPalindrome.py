from builtins import range, len
import sys
sys.path.insert(0, r'..' )
from AbstractAlgorithm import *


class LargestPalindrome(AbstractAlgorithm):

    def __init__(self):
        pass

    def execute(self, data):
        print("Instance of problem:")
        print(data)
        print()
        print("Solution:")
   
        return self.findLargestPalindrome(data)

    def possibility(self, number, occurrences):
        oddOccurrences = 0
        checkedNumbers = []
        for i in range(0, len(number)):
            if(occurrences[number[i]] % 2 != 0 and checkedNumbers.count(number[i]) == 0):
                oddOccurrences += 1
                checkedNumbers.append(number[i])
            if(oddOccurrences > 1):
                return False
        return True

    def findLargestPalindrome(self, number):
        length = len(number)
        digits = list(number)
        index = 0
        checkedDigits = []
        index = 0
        occurrences = {}
        while(index <= length - 1):
            if(checkedDigits.count(number[index]) == 0):
                occurrences.setdefault(number[index],digits.count(number[index]))
                checkedDigits.append(number[index])
            index += 1
        if(self.possibility(number, occurrences) == False):
            return "Palindrome cannot be formed"
        index = 0
        auxValue = ""
        auxPosition = 0
        for i in range(9,-1,-1):
            while(occurrences.get(str(i)) != None and occurrences.get(str(i)) > 1):
                auxValue = digits[index]
                auxPosition = digits.index(str(i),index + 1)
                digits[index] = digits[auxPosition]
                digits[auxPosition] = auxValue
                auxValue = digits[(length - 1) - index]
                auxPosition = digits.index(str(i), index + 1)
                digits[(length - 1) - index] = digits[auxPosition]
                digits[auxPosition] = auxValue
                occurrences.update({str(i):occurrences.get(str(i)) - 2})
                index += 1
        largestPalindrome = ""
        for i in digits:
            largestPalindrome += i
        if(largestPalindrome[0] == "0"):
            return "Palindrome cannot be formed"
        return largestPalindrome

