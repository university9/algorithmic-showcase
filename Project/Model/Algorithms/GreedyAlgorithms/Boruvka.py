import sys

sys.path.insert(0, r'..')
from AbstractAlgorithm import *


# Código fuente de: https://www.geeksforgeeks.org/boruvkas-algorithm-greedy-algo-9/

class Boruvka(AbstractAlgorithm):

    def __init__(self):
       
        pass

    def execute(self, data):
        print(data)
        tempData=eval(data)
        self.V = len(tempData)
        self.graph = list(tempData)
        return self.boruvkaMST()

    def find(self, parent, i):
        if parent[i] == i:
            return i
        return self.find(parent, parent[i])

    def union(self, parent, rank, x, y):
        xroot = self.find(parent, x)
        yroot = self.find(parent, y)

        if rank[xroot] < rank[yroot]:
            parent[xroot] = yroot
        elif rank[xroot] > rank[yroot]:
            parent[yroot] = xroot
        else:
            parent[yroot] = xroot
            rank[xroot] += 1

    def boruvkaMST(self):
        parent = [];
        rank = [];
        cheapest = []
        numTrees = self.V
        MSTweight = 0
        for node in range(self.V):
            parent.append(node)
            rank.append(0)
            cheapest = [-1] * self.V
        while numTrees > 1:
            for i in range(len(self.graph)):
                u, v, w = self.graph[i]
                set1 = self.find(parent, u)
                set2 = self.find(parent, v)
                if set1 != set2:
                    if cheapest[set1] == -1 or cheapest[set1][2] > w:
                        cheapest[set1] = [u, v, w]
                    if cheapest[set2] == -1 or cheapest[set2][2] > w:
                        cheapest[set2] = [u, v, w]
            for node in range(self.V):
                if cheapest[node] != -1:
                    u, v, w = cheapest[node]
                    set1 = self.find(parent, u)
                    set2 = self.find(parent, v)
                    if set1 != set2:
                        MSTweight += w
                        self.union(parent, rank, set1, set2)
                        print ("Camino %d-%d con peso de %d incluido en el AEM" % (u, v, w))
            numTrees = numTrees - 1
            cheapest = [-1] * self.V
        print("El peso del AEM es %d" % MSTweight)
