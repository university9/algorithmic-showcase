import sys
sys.path.insert(0, r'..' )
from AbstractAlgorithm import *

class Item:
    def __init__(self, name, weight, value):
        self.name = name
        self.weight = weight
        self.value = value
        self.profit = value / weight
        self.counter = 0


class Backpack(AbstractAlgorithm):

    def __init__(self):
        pass

    def get_profit(self, val):
            return val.profit

    def list_items(self, data):
        items = []
        for i in range(len(data)):
            print(data[i][0] + ", " + str(data[i][1]) + ", " + str(data[i][2]))
            items.append(Item(data[i][0], data[i][1], data[i][2]))
        items.sort(key= self.get_profit, reverse=True)
        return items

    def knapsack(self, data, weight):

        items = self.list_items(data)
        weight_sum = 0
        a_element = 0

        while (weight_sum < weight and a_element <= len(items) - 1):
            if (items[a_element].weight + weight_sum > weight):
                a_element += 1
            else:
                weight_sum += items[a_element].weight
                items[a_element].counter += 1

        for i in range(len(items)):
            print("El total de " + items[i].name + " optimo es de " + str(items[i].counter))
        print("Para un peso de " + str(weight_sum))

        # for i in range(len(items)):
        #    while(items[i].weight + weight_sum <= weight):
        #        weight_sum += items[i].weight
        #        items[i].counter += 1
        # return items

    def execute(self, data):
        tuple = eval(data)
        return self.knapsack(tuple[0], tuple[1])

