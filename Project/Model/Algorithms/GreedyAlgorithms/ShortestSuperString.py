from builtins import range, len
import sys
sys.path.insert(0, r'..' )
from AbstractAlgorithm import *

#sys.path.insert(0, r'...\Controller' )


class ShortestSuperString(AbstractAlgorithm):

    def __init__(self):
        pass

    def execute(self, data):
        print("Instance of problem:")
        print(data)
        print("Solution:")
        correctData=eval(data)
        return self.findShortestSuperstring(correctData)
    
    def concatenate2Strings(self, str1, str2):
        if str2 in str1:
            return str1
        finded=True
        for n in range(len(str1)):
            if(str1[n]==str2[0]):            
                for j in range(n+1,len(str1)):                
                    if(str1[j]!= str2[j-n]):
                        finded=False
                        break
                if(finded):
                    return str1[0:n]+str2
                finded=True                 
        return str1+str2

    def findBestConcatenation(self, str1,str2):
        strResult1=self.concatenate2Strings(str1,str2)
        strResult2=self.concatenate2Strings(str2,str1)
        if(len(strResult1)>len(strResult2)):
            return strResult2
        return strResult1

    def findShortestSuperstring(self, strList):
        minStr=""
        leftoverStrVals=[]

        while(len(strList)>1):
            for n in range(len(strList)):
                for m in range(n+1,len(strList)):
                    tempStr=self.findBestConcatenation(strList[m],strList[n])
                    if(len(tempStr)<len(minStr) or minStr==""):
                        minStr=tempStr
                        leftoverStrVals=[n,m]

            strList[leftoverStrVals[0]]=minStr
            tempInt=leftoverStrVals[1]
            strList=strList[0:tempInt]+strList[tempInt+1:len(strList)]
            minStr=""
        return strList[0]

