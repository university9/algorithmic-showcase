# Algorithmic Showcase

Para este proyecto se implementaran algoritmos de las siguientes familias:

* Greedy Algorithms
* Divide and Conquer Algorithms
* Dynamic Algorithms
* Genetic Algorithms
* Probabilistic Algorithms

Esto permitirá a los estudiantes adquirir conocimientos para el análisis a priori y luego, aplicar esto, en el proyecto.

Construído en: Python

Grupo 06
2019
